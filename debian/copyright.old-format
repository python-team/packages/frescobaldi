This package was debianized by Ryan Kavanagh <ryanakca@kubuntu.org> on
Sat, 31 Jan 2009 19:53:55 -0500.

Versions prior to 2.0.0 were downloaded from
    http://code.google.com/p/lilykde/downloads/list
Versions 2.0.0 onwards were downloaded from
    https://github.com/wbsoft/frescobaldi/downloads
    

Upstream Authors:
    
    Frescobaldi itself:
        Wilbert Berendsen <info@wilbertberendsen.nl>
    
    Translations:
        cs.po:
            Pavel Fric <pavelfric@seznam.cz>
        de.po:
            Henrik Evers <henrik.evers@gmx.de>
            Georg Hennig <georg.hennig@web.de>
            Urs Liska <git@ursliska.de>
        es.po:
            Francisco Vila <francisco.vila@hispalinux.es>
        fr.po:
            Raphaël Doursenaud <rdoursenaud@free.fr>
            Philippe Massart
            Valentin Villenave <v.villenave@gmail.com>
            David Bouriaud <david.bouriaud@ac-rouen.fr>
            Ryan Kavanagh <ryanakca@kubuntu.org>
            Richard Cognot <cognot@gocat.org>
            Bitouzé Denis <dbitouze@wanadoo.fr>
        gl.po:
            Manuel A. Vazquez <xixirei@yahoo.es>
        it.po:
            Gianluca D'Orazio <gianluca.dorazio@gmail.com>
            .billyBoh <billyboh@gmail.com>
        nl.po:
            Wilbert Berendsen <info@wilbertberendsen.nl>
        pl.po:
            Piotr Komorowski <piotr-komorowski@wp.pl>
        pt_BR.po:
            Édio Mazera <mazera3@gmail.com>
        ru.po:
            Сергей Полтавский <ru@li.org>
            Serj Poltavski <serge.uliss@gmail.com>
            Artem Zolochevskiy <artem.zolochevskiy@gmail.com>
            Mikhail Iglizky <m0003r@gmail.com>
        tr.po:
            Server Acim <sacim@acim.name.tr>
        uk.po:
            Dmytro O. Redchuk <brownian.box@gmail.com>

    ebuild file:
        Gentoo Foundation


Copyright:

    Copyright (C) 2008-2013  Wilbert Berendsen <info@wilbertberendsen.nl>
    Copyright (C) 2009-2013  Pavel Fric <pavelfric@seznam.cz>
    Copyright (C) 2008       Henrik Evers <henrik.evers@gmx.de>
    Copyright (C) 2009-2010  Georg Hennig <georg.hennig@web.de>
    Copyright (C) 2013       Urs Liska <git@ursliska.de>
    Copyright (C) 2008–2013  Francisco Vila <francisco.vila@hispalinux.es>
    Copyright (C) 2011       Raphaël Doursenaud <rdoursenaud@free.fr>
    Copyright (C) 2011       Philippe Massart
    Copyright (C) 2008, 2010 Valentin Villenave <v.villenave@gmail.com>
    Copyright (C) 2009       David Bouriaud <david.bouriaud@ac-rouen.fr>
    Copyright (C) 2009-2011  Ryan Kavanagh <ryanakca@kubuntu.org>
    Copyright (C) 2013       Richard Cognot <cognot@gocad.org>
    Copyright (C) 2013       Bitouzé Denis <dbitouze@wanadoo.fr>
    Copyright (C) 2009–2011  Manuel A. Vazquez <xixirei@yahoo.es>
    Copyright (C) 2008–2013  Gianluca D'Orazio <gianluca.dorazio@gmail.com>
    Copyright (C) 2011–2012  .billyBoh <billyboh@gmail.com>
    Copyright (C) 2009-2012  Piotr Komorowski <piotr-komorowski@wp.pl>
    Copyright (C) 2012       Édio Mazera <mazera3@gmail.com>
    Copyright (C) 2008       Сергей Полтавский <ru@li.org>
    Copyright (C) 2009       Serj Poltavski <serge.uliss@gmail.com>
    Copyright (C) 2008–2010  Server Acim <sacim@acim.name.tr>
    Copyright (C) 2009       Artem Zolochevskiy <artem.zolochevskiy@gmail.com>
    Copyright (C) 2012       Mikhail Iglizky <m0003r@gmail.com>
    Copyright (C) 2012       Dmytro O. Redchuk <brownian.box@gmail.com>
    Copyright (C) 1999–2009  Gentoo Foundation
    
License:

    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this package; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL-2'.

----- Files Deleted From Upstream Tarball ------

 The following files were deleted in the upstream tarball to avoid both taking
 up unneeded space due to file duplication and needlessly tracking copyright
 holders:

  * frescobaldi_app/hyphdict

  * frescobaldi_app/icons/Tango

------------------------------------------------

The following files have different licenses or copyright holders:

----- frescobaldi_app/unicode_blocks.py ------

Contains Unicode Block data which was retrieved from
http://www.unicode.org/Public/5.2.0/ucd/Blocks.txt

with the following copyright information:

Copyright (c) 1991-2009 Unicode, Inc.
For terms of use, see http://www.unicode.org/terms_of_use.html

The corresponding license is as follows:

  EXHIBIT 1
  UNICODE, INC. LICENSE AGREEMENT - DATA FILES AND SOFTWARE

   Unicode Data Files include all data files under the directories
   http://www.unicode.org/Public/, http://www.unicode.org/reports/,
   and http://www.unicode.org/cldr/data/ . Unicode Software includes any
   source code published in the Unicode Standard or under the directories
   http://www.unicode.org/Public/, http://www.unicode.org/reports/,
   and http://www.unicode.org/cldr/data/.

      NOTICE TO USER: Carefully read the following legal agreement. BY
      DOWNLOADING, INSTALLING, COPYING OR OTHERWISE USING UNICODE
      INC.'S DATA FILES ("DATA FILES"), AND/OR SOFTWARE ("SOFTWARE"), YOU
      UNEQUIVOCALLY ACCEPT, AND AGREE TO BE BOUND BY, ALL OF THE TERMS AND
      CONDITIONS OF THIS AGREEMENT. IF YOU DO NOT AGREE, DO NOT DOWNLOAD,
      INSTALL, COPY, DISTRIBUTE OR USE THE DATA FILES OR SOFTWARE.

      COPYRIGHT AND PERMISSION NOTICE

      Copyright © 1991-2012 Unicode, Inc. All rights
      reserved. Distributed under the Terms of Use in
      http://www.unicode.org/copyright.html.

      Permission is hereby granted, free of charge, to any person
      obtaining a copy of the Unicode data files and any associated
      documentation (the "Data Files") or Unicode software and any
      associated documentation (the "Software") to deal in the Data Files
      or Software without restriction, including without limitation
      the rights to use, copy, modify, merge, publish, distribute,
      and/or sell copies of the Data Files or Software, and to permit
      persons to whom the Data Files or Software are furnished to do so,
      provided that (a) the above copyright notice(s) and this permission
      notice appear with all copies of the Data Files or Software,
      (b) both the above copyright notice(s) and this permission notice
      appear in associated documentation, and (c) there is clear notice
      in each modified Data File or in the Software as well as in the
      documentation associated with the Data File(s) or Software that
      the data or software has been modified.

      THE DATA FILES AND SOFTWARE ARE PROVIDED "AS IS", WITHOUT WARRANTY
      OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
      AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE
      COPYRIGHT HOLDER OR HOLDERS INCLUDED IN THIS NOTICE BE LIABLE
      FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES,
      OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
      PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
      TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
      PERFORMANCE OF THE DATA FILES OR SOFTWARE.

      Except as contained in this notice, the name of a copyright holder
      shall not be used in advertising or otherwise to promote the sale,
      use or other dealings in these Data Files or Software without
      prior written authorization of the copyright holder.

----------------------------------------------

---------- frescobaldi_app/icons/ ------------

 The following files:
    * application-pdf.svg
        Author: Jakub Steiner
    * pushpin.svg
        Author: jhnri4
        Retrieved From: http://openclipart.org/detail/89059
    * text-x-lilypond.svg
        Author: Jakub Steiner

 are in the public domain as stipulated by the <cc:License
 rdf:about="http://creativecommons.org/licenses/publicdomain/"> tags contained
 therein.

 This page states that:

        The person or persons who have associated work with this document (the
        "Dedicator" or "Certifier") hereby either (a) certifies that, to the
        best of his knowledge, the work of authorship identified is in the
        public domain of the country from which the work is published, or (b)
        hereby dedicates whatever copyright the dedicators holds in the work of
        authorship identified below (the "Work") to the public domain. A
        certifier, moreover, dedicates any copyright interest he may have in the
        associated work, and for these purposes, is described as a "dedicator"
        below.

        A certifier has taken reasonable steps to verify the copyright status of
        this work. Certifier recognizes that his good faith efforts may not
        shield him from liability if in fact the work certified is not in the
        public domain.

        Dedicator makes this dedication for the benefit of the public at large
        and to the detriment of the Dedicator's heirs and successors. Dedicator
        intends this dedication to be an overt act of relinquishment in
        perpetuity of all present and future rights under copyright law, whether
        vested or contingent, in the Work. Dedicator understands that such
        relinquishment of all rights includes the relinquishment of all rights
        to enforce (by lawsuit or otherwise) those copyrights in the Work.

        Dedicator recognizes that, once placed in the public domain, the Work
        may be freely reproduced, distributed, transmitted, used, modified,
        built upon, or otherwise exploited by anyone for any purpose, commercial
        or non-commercial, and in any way, including by methods that have not
        yet been invented or conceived.

 --

 The following files:
   * edit-clear-locationbar-rtl.svg
   * tools-score-wizard.svg

 are from the Oxygen icon theme which is

    Copyright (C) 2007 The Oxygen Team

 under the following license:

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.

Clarification:

  The GNU Lesser General Public License or LGPL is written for
  software libraries in the first place. We expressly want the LGPL to
  be valid for this artwork library too.

  KDE Oxygen theme icons is a special kind of software library, it is an
  artwork library, it's elements can be used in a Graphical User Interface, or
  GUI.

  Source code, for this library means:
   - where they exist, SVG;
   - otherwise, if applicable, the multi-layered formats xcf or psd, or
  otherwise png.

  The LGPL in some sections obliges you to make the files carry
  notices. With images this is in some cases impossible or hardly useful.

  With this library a notice is placed at a prominent place in the directory
  containing the elements. You may follow this practice.

  The exception in section 5 of the GNU Lesser General Public License covers
  the use of elements of this art library in a GUI.

  kde-artists [at] kde.org

 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-3'.

 --

 The following files under TangoExt/ are derived from the Tango icon theme:

  * application-exit.svg: renamed from system-log-out.svg
  * document-close.svg: derived from document-new.svg and
                                     audio-volume-muted.svg
  * help-about.svg: renamed from help-browser.svg
  * text-plain.svg: renamed from text-x-generic.svg

 The Tango icon theme is in the public domain and refers to
 http://creativecommons.org/licenses/publicdomain/ for details. The contents of
 this page are stated above.

 The remaining changes to those were made by Wilbert Berendsen and fall under
 the same license as Frescobaldi.

 The following files under TangoExt/ have different sources licenses:

  * configure-shortcuts.svg:
      Retrieved From: http://commons.wikimedia.org/wiki/File:Preferences-desktop-keyboard-shortcuts.svg
      Author: Tango! Desktop Project
      License: Public Domain, see above.
  * document-edit.svg:
      Retrieved From: http://commons.wikimedia.org/wiki/File:Edit-find-replace.svg
      Author: Tango! Desktop Project, modified by Wilbert Berendsen
      License: Original icon Public Domain, remaining changes under same license
               as Frescobaldi.
  * document-save-all.svg:
      Author: Jakub Steiner
      License: Public Domain, see above.
  * help-contents.svg:
      Retrieved From: http://commons.wikimedia.org/wiki/File:Help-contents.svg
      Author: Tango! Desktop Project
      License: Public Domain, see above.
  * preferences-other.svg:
      Retrieved From: http://commons.wikimedia.org/wiki/File:Gnome-preferences-other.svg
      Author: GNOME icon artists
      License:
         This program is free software: you can redistribute it and/or modify it
         under the terms of the GNU General Public License as published by the
         Free Software Foundation, either version 3 of the License, or (at your
         option) any later version.
          
         This package is distributed in the hope that it will be useful, but
         WITHOUT ANY WARRANTY; without even the implied warranty of
         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
         General Public License for more details.
          
         You should have received a copy of the GNU General Public License along
         with this program. If not, see <http://www.gnu.org/licenses/>.
          
         On Debian systems, the complete text of the GNU General Public License
         version 3 can be found in "/usr/share/common-licenses/GPL-3".
  * zoom-in.svg, zoom-out.svg, zoom-original.svg:
      Author: Lapo Calamandrei
      Contributor: Jakub Steiner
      License: GPL 2.0

----------------------------------------------

The Debian packaging is Copyright (C) 2009–2013 Ryan Kavanagh <rak@debian.org>
and is licensed under the GPL version 2, or (at your option) any later version,
see above.
