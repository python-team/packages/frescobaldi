Source: frescobaldi
Section: editors
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ryan Kavanagh <rak@debian.org>,
           Anthony Fok <foka@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3,
               python3-ly (>= 0.9.5),
               python3-setuptools,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/frescobaldi
Vcs-Git: https://salsa.debian.org/python-team/packages/frescobaldi.git
Homepage: https://www.frescobaldi.org/

Package: frescobaldi
Architecture: any
Depends: python3-ly (>= 0.9.5),
         python3-poppler-qt5,
         python3-pygame,
         python3-pyqt5,
         python3-pyqt5.qtsvg,
         python3-pyqt5.qtwebengine [amd64 arm64 armhf i386 mips64el],
         python3-qpageview,
         tango-icon-theme,
         ${misc:Depends},
         ${python3:Depends}
Recommends: lilypond (>= 2.24.1),
            fluidsynth
Suggests: lilypond-doc,
          hyphen-hyphenation-patterns
Enhances: lilypond
Description: LilyPond sheet music text editor
 Frescobaldi is a LilyPond sheet music editor.  It aims to be powerful,
 yet lightweight and easy to use.  Frescobaldi is Free Software, freely
 available under the GNU General Public License.
 .
 Features:
 .
  * Powerful text editor with syntax highlighting and automatic completion
  * Music view with advanced Point & Click
  * MIDI player to proof-listen LilyPond-generated MIDI files
  * MIDI capturing to enter music
  * Powerful Score Wizard to quickly setup a music score
  * Snippet Manager to store and apply text snippets, templates or scripts
  * Use multiple versions of LilyPond, automatically selects the correct version
  * Built-in LilyPond documentation browser and built-in Frescobaldi User Guide
  * Smart layout-control functions like coloring specific objects in the PDF
  * MusicXML, MIDI and ABC import
  * Modern user interface with configurable colors, fonts and keyboard shortcuts
  * Translated into Dutch, English, French, German, Italian, Czech, Russian,
    Spanish, Galician, Turkish, Polish, Brazillian Portuguese and Ukrainian.
 .
 Music functions:
 .
  * Transpose music
  * Change music from relative to absolute and vice versa
  * Change the language used for note names
  * Change the rhythm (double, halve, add/remove dots, copy, paste) etc.
  * Hyphenate lyrics using word-processor hyphenation dictionaries
  * Add spanners, dynamics, articulation easily using the Quick Insert panel
  * Update LilyPond syntax using convert-ly, with display of differences
 .
 In order to use lyric hyphenation, please install your languages' appropriate
 hyphenation packages, e.g. hyphen-fr for French, hyphen-ca for Catalan, etc.
 .
 Frescobaldi is designed to run on all major operating systems (Linux, macOS
 and MS Windows).  It is named after Girolamo Frescobaldi (1583-1643), an
 Italian composer of keyboard music in the late Renaissance and early Baroque
 period.
